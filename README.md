# 2D Sidescroller Demo / Tutorial in Simulant Engine

## Uses
- Simulant Engine - [https://gitlab.com/simulant/simulant](https://gitlab.com/simulant/simulant)
  - Cross Platform: Windows, Linux, Android, and Sega Dreamcast.
- C++11 (barely)

## Outline
Follows this tutorial basically commit by commit for lines in the article

- [Basic 2D Platformer Physics, Part 1](https://gamedevelopment.tutsplus.com/tutorials/basic-2d-platformer-physics-part-1--cms-25799)
- [Basic 2D Platformer Physics, Part 2](https://gamedevelopment.tutsplus.com/tutorials/basic-2d-platformer-physics-part-2--cms-25922)
- [How to Create a Custom 2D Physics Engine: Friction, Scene and Jump Table](https://gamedevelopment.tutsplus.com/tutorials/how-to-create-a-custom-2d-physics-engine-friction-scene-and-jump-table--gamedev-7756)

## Goal
Give a basic easy to follow tutorial and example for a complete game using Simulant Engine.