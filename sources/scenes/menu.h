#pragma once

#include <simulant/simulant.h>
#include <simulant/shortcuts.h>
#include <simulant/extra.h>
using namespace smlt;

class MenuScene : public smlt::Scene<MenuScene> {
public:
    // Boilerplate
    MenuScene(smlt::Window* window):
        smlt::Scene<MenuScene>(window) {}

    void load();
    void update(float dt);
    void activate();
    void deactivate();
    void unload();
private:
    CameraPtr camera_;
    StagePtr stage_;
    TextureID text_tex;
    MaterialPtr text_mat;
    
    //Actors and Meshes
    MeshID start_text;
    ActorPtr start_actor;

    PipelinePtr pipeline_;
};
