#pragma once

#include <simulant/simulant.h>
#include <simulant/shortcuts.h>
#include <simulant/extra.h>
using namespace smlt;

class GameScene : public smlt::Scene<GameScene> {
public:
    // Boilerplate
    GameScene(smlt::Window* window):
        smlt::Scene<GameScene>(window) {}

    void load();
    void update(float dt);
    void activate();
    void deactivate();
    void unload();
private:
    CameraPtr camera_;
    StagePtr stage_;
   
    //Actors and Meshes
    MeshID ground_mesh_id_;
    ActorPtr ground_;

    MeshID box_mesh_id_;
    ActorPtr actor_;

    PipelinePtr pipeline_;
};
