
#include "menu.h"
#include "game.h"
#include "../behaviors/Simple3DText.h"

void MenuScene::load() {

    //Start Loading our Game
    scenes->register_scene<GameScene>("game");
    scenes->load_in_background("game", false); //Do loading in a background thread, but show immediately when done
    
    prepare_basic_scene(stage_, camera_);

    //Prepare Simple Pipline
    pipeline_ = prepare_basic_scene(stage_, camera_, smlt::PARTITIONER_NULL);
    pipeline_->deactivate();
    pipeline_->viewport->set_colour(smlt::Colour::SKY_BLUE);

    //Add our Camera and then Move Slightly Up
    camera_->set_perspective_projection(Degrees(45.0), float(window->width()) / float(window->height()), 1.0, 1000.0);
    camera_->move_to(Vec3(0, 5, 0));

    stage_->set_ambient_light(smlt::Colour(1.0, 1.0, 1.0, 1.0));

    auto light = stage_->new_light_as_point(Vec3(5, 0, -5), smlt::Colour::GREEN);
    light->set_attenuation_from_range(30.0);

    //Load our 3D Text mesh
    /*start_text = stage_->assets->new_mesh_from_file("start.obj");
    //Load the Texture for it
    text_tex = stage_->assets->new_texture_from_file("untitled.png");
    //Create a material from it and then assign the material to the mesh
    text_mat = stage_->assets->new_material_from_texture(text_tex).fetch();
    stage_->assets->mesh(start_text)->set_material_id(text_mat->id());*/

    auto test = stage_->new_actor();
    test->new_behaviour<smlt::behaviours::Simple3DText>("start");
    test->move_to(Vec3(-10,22,-60));

    auto test2 = stage_->new_actor();
    auto new_behav = test2->new_behaviour<smlt::behaviours::Simple3DText>("options", "stop");
    auto text_tex = window->shared_assets->new_texture_from_file("blue.png");
    //Create a material from it and then assign the material to the mesh
    auto text_mat = window->shared_assets->new_material_from_texture(text_tex).fetch();
    new_behav->set_material_id(text_mat->id());
    test2->move_to(Vec3(-10,12,-60));

    auto test3 = stage_->new_actor();
    test3->new_behaviour<smlt::behaviours::Simple3DText>("about");
    test3->move_to(Vec3(-10,2,-60));
}

void MenuScene::update(float dt) {
    // Update your scene here

    //Check if we  move to game
    if (input->axis_value("Fire1") >= 0.99f)
        {
            //if(scenes->is_loaded("game")){
                scenes->activate("game");
            //}
        }
}

void MenuScene::activate() {
    //Required to use the pipeline
    pipeline_->activate();
}

void MenuScene::deactivate() {
    // Deactviate any render pipelines here
}

void MenuScene::unload() {
    // Cleanup your scene here
    start_actor->ask_owner_for_destruction();
}
