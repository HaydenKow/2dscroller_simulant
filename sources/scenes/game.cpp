
#include "game.h"
#include "../behaviors/QuickCollisionPlayer.h"

void GameScene::load() {

    prepare_basic_scene(stage_, camera_);

    //Prepare Simple Pipline
    pipeline_ = prepare_basic_scene(stage_, camera_, smlt::PARTITIONER_NULL);
    pipeline_->deactivate();
    pipeline_->viewport->set_colour(smlt::Colour::SKY_BLUE);

    //Add our Camera and then Move Slightly Up
    camera_->set_perspective_projection(Degrees(45.0), float(window->width()) / float(window->height()), 1.0, 1000.0);
    camera_->move_to(Vec3(0, 2, 0));

    //Setup a decent Skybox
    stage_->skies->new_skybox_from_folder("skyboxes/TropicalSunnyDay");

    //Create Ground Using the same techniques
    smlt::TextureID sand_tex = window->shared_assets->new_texture_from_file("beach_sand.png");
    ground_mesh_id_ = window->shared_assets->new_mesh_as_box(200, 2, 500);
    window->shared_assets->mesh(ground_mesh_id_)->set_material_id(window->shared_assets->new_material_from_texture(sand_tex));
    ground_ = stage_->new_actor_with_mesh(ground_mesh_id_);
    ground_->move_to(0,-2.5,0);

    //Texture for Player
    /*smlt::TextureID crate = window->shared_assets->new_texture_from_file("crate.png");
    smlt::MaterialID mat = window->shared_assets->new_material_from_texture(crate);
    //Mesh for Player
    box_mesh_id_ = window->shared_assets->new_mesh_as_box(1, 1, 1, smlt::GARBAGE_COLLECT_NEVER);
    window->shared_assets->mesh(box_mesh_id_)->set_material_id(mat);*/

    //Create our Player
    smlt::MeshID mesh_id = stage_->assets->new_mesh_from_file("rhino/tris.md2");
    actor_ = stage_->new_actor_with_mesh(mesh_id);
    actor_->move_to(Vec3(0, 5, -20));
    actor_->scale_by(Vec3(0.06f,0.06f,0.06f));
    actor_->rotate_global_y_by(smlt::Degrees(90));
    actor_->animation_state->play_animation("stand");
    actor_->new_behaviour<smlt::behaviours::QuickCollisionPlayer>(window);
}

void GameScene::update(float dt) {
    // Update your scene here
}

void GameScene::activate() {
    //Required to use the pipeline
    pipeline_->activate();
}

void GameScene::deactivate() {
    // Deactviate any render pipelines here
}

void GameScene::unload() {
    // Cleanup your scene here
}
