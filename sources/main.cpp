#include <simulant/simulant.h>

#include "scenes/menu.h"

using namespace smlt;

class Testproj2:
    public Application {

public:
    Testproj2(const AppConfig& config):
        Application(config) {}

    bool init() {
        //Load our Menu
        scenes->register_scene<MenuScene>("menu");
        scenes->load_in_background("menu", true); //Do loading in a background thread, but show immediately when done
        scenes->register_scene<smlt::scenes::Splash>("main", "menu");
        return true;
    }
};

extern "C" {

#ifdef __ANDROID__

#include <SDL_main.h>
#include <gpg/gpg.h>

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    gpg::AndroidInitialization::JNI_OnLoad(vm);
    return JNI_VERSION_1_6;
}

    int SDL_main(int argc, char* argv[])
#else
    int main(int argc, char* argv[])
#endif
{
    AppConfig config;
    config.title = "2D Collision";
    config.search_paths.push_back("assets");

    // Dreamcast only
    config.search_paths.push_back("/cd/assets");

#ifdef DEBUG
    config.log_level = smlt::LOG_LEVEL_DEBUG;
#endif

    config.fullscreen = false;
    config.width = 800;
    config.height = 600;

    Testproj2 app(config);
    return app.run();
}

}
