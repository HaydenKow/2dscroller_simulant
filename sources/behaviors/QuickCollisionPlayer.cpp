/*
 * Filename: /mnt/hgfs/share/testproj2/sources/behaviors/QuickCollisionAABB.cpp
 * Path: /mnt/hgfs/share/testproj2/sources/behaviors
 * Created Date: Tuesday, January 8th 2019, 6:57:01 pm
 * Author: dev
 * 
 * Copyright (c) 2019 Your Company
 */
#include "QuickCollisionPlayer.h"
#include <simulant/simulant.h>

namespace smlt
{
namespace behaviours
{

QuickCollisionPlayer::QuickCollisionPlayer(Window *window) : QuickCollisionObject(window)
{
}

void QuickCollisionPlayer::on_behaviour_first_update(Organism *owner)
{
}

void QuickCollisionPlayer::on_behaviour_added(Organism *controllable)
{
    QuickCollisionObject::on_behaviour_added(controllable);
    stage_node_ = dynamic_cast<Actor*>(controllable);

    //Set initial state to standing
    currentState = CharacterState::Stand;
    position = Vec2(stage_node_->position().x, stage_node_->position().y);
    _AABB.halfSize = Vec2(stage_node_->aabb().width() / 2, stage_node_->aabb().height() / 2);
    AABBOffset.y = _AABB.halfSize.y+0.10f;

    inputs = new bool[(int)KeyInput::Count];
    prevInputs = new bool[(int)KeyInput::Count];

    jumpSpeed = Constants::cJumpSpeed;
    walkSpeed = Constants::cWalkSpeed;

    scale = Vec2(1, 1);
}

void QuickCollisionPlayer::on_behaviour_removed(Organism *controllable)
{
    QuickCollisionObject::on_behaviour_removed(controllable);
    stage_node_ = nullptr;
}

void QuickCollisionPlayer::update(float dt)
{
    if (!attached())
    {
        return;
    }
    QuickCollisionObject::update(dt);

    inputs[(int)KeyInput::GoRight] = input->axis_value("Horizontal") > 0.0f;
    inputs[(int)KeyInput::GoLeft] = input->axis_value("Horizontal") < 0.0f;
    inputs[(int)KeyInput::GoDown] = input->axis_value("Vertical") > 0.0f;
    inputs[(int)KeyInput::Jump] = input->axis_value("Vertical") > 0.0f;

    //Big Update switchboard
    switch (currentState)
    {
    case CharacterState::Stand:

        /*mWalkSfxTimer = cWalkSfxTime;*/
        stage_node_-> animation_state->play_animation("idle_1");

        speed = Vec2();

        if (!onGround)
        {
            currentState = CharacterState::Jump;
            break;
        }

        //if left or right key is pressed, but not both
        if (key_state(KeyInput::GoRight) != key_state(KeyInput::GoLeft))
        {
            currentState = CharacterState::Walk;
            break;
        }
        else if (key_state(KeyInput::Jump))
        {
            speed.y = jumpSpeed;
            //mAudioSource.PlayOneShot(mJumpSfx);
            currentState = CharacterState::Jump;
            break;
        }

        break;
    case CharacterState::Walk:
        stage_node_-> animation_state->play_animation("running");
            /*mWalkSfxTimer += Time.deltaTime;*/

        /*if (mWalkSfxTimer > cWalkSfxTime)
            {
                mWalkSfxTimer = 0.0f;
                mAudioSource.PlayOneShot(mWalkSfx);
            }*/

        //if both or neither left nor right keys are pressed then stop walking and stand

        if (key_state(KeyInput::GoRight) == key_state(KeyInput::GoLeft))
        {
            currentState = CharacterState::Stand;
            speed = Vec2();
            break;
        }
        else if (key_state(KeyInput::GoRight))
        {
            if (pushesRightWall)
                speed.x = 0.0f;
            else
                speed.x = walkSpeed;

            //Flip Character
            stage_node_->rotate_to_absolute(smlt::Degrees(270),0,1.0f,0);
        }
        else if (key_state(KeyInput::GoLeft))
        {
            
            if (pushesLeftWall)
                speed.x = 0.0f;
            else
                speed.x = -walkSpeed;
            //Flip Character
            stage_node_->rotate_to_absolute(smlt::Degrees(90),0,1.0f,0);
        }

        //if there's no tile to walk on, fall
        if (key_state(KeyInput::Jump))
        {
            speed.y = jumpSpeed;
            //mAudioSource.PlayOneShot(mJumpSfx, 1.0f);
            currentState = CharacterState::Jump;
            stage_node_-> animation_state->play_animation("jumping");
            break;
        }
        else if (!onGround)
        {
            currentState = CharacterState::Jump;
            stage_node_-> animation_state->play_animation("jumping");
            break;
        }

        break;
    case CharacterState::Jump:
        
        /*mWalkSfxTimer = cWalkSfxTime;*/

        speed.y += Constants::cGravity * dt;

        //speed.y =  speed.y < Constants::cMaxFallingSpeed ? Constants::cMaxFallingSpeed : speed.y;

        if (!key_state(KeyInput::Jump) && speed.y > 0.0f)
        {
            speed.y = std::min(speed.y, 20.0f);
        }

        if (key_state(KeyInput::GoRight) == key_state(KeyInput::GoLeft))
        {
            speed.x = 0.0f;
        }
        else if (key_state(KeyInput::GoRight))
        {
            if (pushesRightWall)
                speed.x = 0.0f;
            else
                speed.x = walkSpeed;
            //Flip
            stage_node_->rotate_to_absolute(smlt::Degrees(270),0,1.0f,0);
        }
        else if (key_state(KeyInput::GoLeft))
        {
            if (pushesLeftWall)
                speed.x = 0.0f;
            else
                speed.x = -walkSpeed;
            //Flip
            stage_node_->rotate_to_absolute(smlt::Degrees(90),0,1.0f,0);
        }

        //if we hit the ground
        if (onGround)
        {
            //if there's no movement change state to standing
            if (inputs[(int)KeyInput::GoRight] == inputs[(int)KeyInput::GoLeft])
            {
                currentState = CharacterState::Stand;
                speed = Vec2();
                //mAudioSource.PlayOneShot(mHitWallSfx, 0.5f);
            }
            else //either go right or go left are pressed so we change the state to walk
            {
                currentState = CharacterState::Walk;
                speed.y = 0.0f;
                //mAudioSource.PlayOneShot(mHitWallSfx, 0.5f);
            }
        }
        break;

    case CharacterState::GrabLedge:
        break;
    }

    if ((!wasOnGround && onGround) || (!wasAtCeiling && atCeiling) || (!pushedLeftWall && pushesLeftWall) || (!pushedRightWall && pushesRightWall))
    {
        //mAudioSource.PlayOneShot(mHitWallSfx, 0.5f);
    }

    UpdatePrevInputs();
}

void QuickCollisionPlayer::fixed_update(float step)
{
}

void QuickCollisionPlayer::UpdatePrevInputs()
{
    char count = (char)KeyInput::Count;

    for (unsigned char i = 0; i < count; ++i)
        prevInputs[i] = inputs[i];
}

bool QuickCollisionPlayer::released(KeyInput key)
{
    return (!inputs[(int)key] && prevInputs[(int)key]);
}

bool QuickCollisionPlayer::key_state(KeyInput key)
{
    return (inputs[(int)key]);
}

bool QuickCollisionPlayer::pressed(KeyInput key)
{
    return (inputs[(int)key] && !prevInputs[(int)key]);
}
}
}
