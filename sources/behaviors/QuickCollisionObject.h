/*
 * Filename: /mnt/hgfs/share/testproj2/sources/behaviors/QuickCollisionAABB.cpp
 * Path: /mnt/hgfs/share/testproj2/sources/behaviors
 * Created Date: Tuesday, January 8th 2019, 6:57:01 pm
 * Author: dev
 * 
 * Copyright (c) 2019 Your Company
 */

#pragma once

#include <simulant/simulant.h>
#include <simulant/window.h>
#include <simulant/behaviours/behaviour.h>
#include <simulant/interfaces/transformable.h>

#include "QuickCollisionManager.h"

namespace smlt
{

class Window;

namespace behaviours
{

class QuickCollisionObject : public BehaviourWithInput
{

  public:
    QuickCollisionObject(Window *window);

    const std::string name() const
    {
        return "QuickCollisionObject";
    }

    void update(float dt);
    void fixed_update(float step);

    //Physics Vars
    Vec2 oldPosition;
    Vec2 position;
    
    Vec2 oldSpeed;
    Vec2 speed;
    
    Vec2 scale;
    QuickAABB _AABB;
    Vec2 AABBOffset;

    //Physics Vars to know current and past state
    bool pushedRightWall;
    bool pushesRightWall;

    bool pushedLeftWall;
    bool pushesLeftWall;

    bool wasOnGround;
    bool onGround;

    bool wasAtCeiling;
    bool atCeiling;

  protected:
    void on_behaviour_first_update(Organism *owner);
    void on_behaviour_added(Organism *controllable) override;
    void on_behaviour_removed(Organism *controllable) override;

    StageNode *stage_node_ = nullptr;

    bool attached() const { return stage_node_ != nullptr; }

  protected:
};
}
}
