/*
 * Filename: /mnt/hgfs/share/testproj2/sources/behaviors/QuickCollisionAABB.cpp
 * Path: /mnt/hgfs/share/testproj2/sources/behaviors
 * Created Date: Tuesday, January 8th 2019, 6:57:01 pm
 * Author: dev
 * 
 * Copyright (c) 2019 Your Company
 */
#include "QuickCollisionManager.h"
#include <simulant/simulant.h>

namespace smlt
{
namespace behaviours
{

QuickCollisionManager::QuickCollisionManager()
{
}

void QuickCollisionManager::on_behaviour_first_update(Organism *owner)
{
}

void QuickCollisionManager::on_behaviour_added(Organism *controllable)
{
    stage_node_ = dynamic_cast<StageNode *>(controllable);
}

void QuickCollisionManager::on_behaviour_removed(Organism *controllable)
{
    stage_node_ = nullptr;
}

void QuickCollisionManager::update(float dt)
{
}

void QuickCollisionManager::fixed_update(float step)
{
}
}
}
