/*
 * Filename: /mnt/hgfs/share/testproj2/sources/behaviors/QuickCollisionAABB.cpp
 * Path: /mnt/hgfs/share/testproj2/sources/behaviors
 * Created Date: Tuesday, January 8th 2019, 6:57:01 pm
 * Author: dev
 * 
 * Copyright (c) 2019 Your Company
 */
#include "QuickCollisionObject.h"
#include <simulant/simulant.h>

namespace smlt
{
namespace behaviours
{

QuickCollisionObject::QuickCollisionObject(Window *window) : BehaviourWithInput(window->input.get())
{
}

void QuickCollisionObject::on_behaviour_first_update(Organism *owner)
{
}

void QuickCollisionObject::on_behaviour_added(Organism *controllable)
{
    stage_node_ = dynamic_cast<StageNode *>(controllable);
}

void QuickCollisionObject::on_behaviour_removed(Organism *controllable)
{
    stage_node_ = nullptr;
}

void QuickCollisionObject::update(float dt)
{
    if (!attached())
    {
        return;
    }
    oldPosition = position;
    oldSpeed = speed;

    wasOnGround = onGround;
    pushedRightWall = pushesRightWall;
    pushedLeftWall = pushesLeftWall;
    wasAtCeiling = atCeiling;

    Vec3 change = Vec3(speed.x * dt, speed.y * dt, 0);
    stage_node_->move_by(change);
    position += speed*dt;
    /*stage_node_->scale_by(Vec3(scale.x, scale.y, 1.0f));
    scale = Vec3();*/

    if (position.y <= 0.0f)
    {
        position.y = 0.0f;
        onGround = true;
    }
    else
        onGround = false;

    _AABB.center = position + AABBOffset;
}

void QuickCollisionObject::fixed_update(float step)
{
}
}
}
