/*
 * Created Date: 1/14/2019, 12:22:03 PM
 * Author:  ()
 * 
 * Copyright (c) 2019 Your Company
 */

#include "Simple3DText.h"
#include <simulant/simulant.h>

namespace smlt
{
namespace behaviours
{

Simple3DText::Simple3DText(std::string _string, std::string _font)
{
    letters_ = _string;
    meshes_ = new MeshID[letters_.length()]();
    actors_ = new ActorPtr[letters_.length()]();
    font_ = _font;
}

void Simple3DText::set_material_id(MaterialID mat) {
     for (int i = 0; i < length_; i++)
    {
        meshes_[i].fetch()->set_material_id(mat);
    }
}

void Simple3DText::on_behaviour_first_update(Organism *owner)
{
}

void Simple3DText::on_behaviour_added(Organism *controllable)
{
    stage_node_ = dynamic_cast<StageNode *>(controllable);
    stage_ = stage_node_->stage;
    length_ = letters_.length();

    //Load the Texture for it
    letter_tex = stage_->window->shared_assets->texture(stage_->window->shared_assets->get_texture_with_alias("orange"));
    if(!letter_tex){
        printf("loading texture\n");
        letter_tex = stage_->window->shared_assets->new_texture_with_alias_from_file("orange","orange.png").fetch();
    }
    
    letter_mat = stage_->window->shared_assets->new_material_from_texture(letter_tex->id()).fetch();

    float x_offset = 0;
    char filename[32];
    for (int i = 0; i < length_; i++)
    {
        sprintf(filename, "%s/%c.obj", font_.c_str(), (char)toupper(letters_.c_str()[i]));

        //Load our 3D Text mesh
        meshes_[i] = stage_->assets->new_mesh_from_file(filename);
        stage_->assets->mesh(meshes_[i])->set_material_id(letter_mat->id());

        //Create a full Actor from the textured Mesh
        actors_[i] = stage_->new_actor_with_mesh(meshes_[i]);
        stage_node_->add_child(actors_[i]);
        //actors_[i]->move_by(Vec3(x_offset,0,0));
        actors_[i]->scale_by(Vec3(0.1f, 0.1f, 0.1f));
        actors_[i]->rotate_x_by(Degrees(-90.0f));

        //Fix spacing between letters for futura
        float temp_offset = x_offset / (i + 1);
        if (i >= 1)
        {
            temp_offset = meshes_[i].fetch()->aabb().width() * 0.1f + 1.0f;
        }
        x_offset += (temp_offset < (x_offset / (i + 1) * 1.3)) ? x_offset / (i + 1) + 2.0f : temp_offset;
        actors_[i]->move_to(Vec3(x_offset, 0, 0));
    }
    stage_node_->move_right_by(-x_offset / 2.0f);
}

void Simple3DText::on_behaviour_removed(Organism *controllable)
{
    stage_node_ = nullptr;

    //cleanup
}

void Simple3DText::update(float dt)
{
    float speed = 40.0f;
    float scale = 2.5f;
    if (!attached() && length_ == 0)
    {
        return;
    }
    elapsed += dt;

    for (int i = 0; i < length_; i++)
    {
        if (actors_[i] != NULL)
        {
            Vec3 pos = actors_[i]->position();
            actors_[i]->move_to(Vec3(pos.x, sin((i - elapsed) / 6 * speed) * scale, pos.z));
        }
    }
}

void Simple3DText::fixed_update(float step)
{
}
}
}
