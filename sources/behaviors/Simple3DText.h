/*
 * Created Date: 1/14/2019, 12:23:10 PM
 * Author:  ()
 * 
 * Copyright (c) 2019 Your Company
 */

#pragma once

#include <simulant/simulant.h>
#include <simulant/window.h>
#include <simulant/behaviours/behaviour.h>

namespace smlt
{

class Window;

namespace behaviours
{

class Simple3DText : public Behaviour, public Managed<Simple3DText>
{

  public:
    Simple3DText(std::string _string) : Simple3DText(_string, "futura") {};
    Simple3DText(std::string _string, std::string _font);

    const std::string name() const
    {
        return "Simple3DText";
    }

    float elapsed = 0.0f;

    void update(float dt);
    void fixed_update(float step);

    void set_material_id(MaterialID mat);

    //Doesnt't do anything yet
    std::string get_font(){return font_;}
    void set_font(std::string font);

  private:
    void on_behaviour_first_update(Organism *owner);
    void on_behaviour_added(Organism *controllable) override;
    void on_behaviour_removed(Organism *controllable) override;

    StageNode *stage_node_ = nullptr;

    bool attached() const { return stage_node_ != nullptr; }
    std::string letters_;
    std::string font_;
    int length_ =0;

    //Keep our stage pointer and arrays for all letters in this word
    StagePtr    stage_   = nullptr;
    MeshID      *meshes_ = nullptr;
    ActorPtr    *actors_ = nullptr;


    //Share one texture/Material
    TexturePtr   letter_tex;
    MaterialPtr letter_mat = nullptr;

  protected:
};
}
}
