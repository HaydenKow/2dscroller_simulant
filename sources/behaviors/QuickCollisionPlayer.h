/*
 * Filename: /mnt/hgfs/share/testproj2/sources/behaviors/QuickCollisionAABB.cpp
 * Path: /mnt/hgfs/share/testproj2/sources/behaviors
 * Created Date: Tuesday, January 8th 2019, 6:57:01 pm
 * Author: dev
 * 
 * Copyright (c) 2019 Your Company
 */

#pragma once

#include <simulant/simulant.h>
#include <simulant/window.h>
#include <simulant/behaviours/behaviour.h>
#include <simulant/interfaces/transformable.h>

#include "QuickCollisionObject.h"

enum class KeyInput
{
  GoLeft = 0,
  GoRight,
  GoDown,
  Jump,
  Count
};

//Define our character states
enum class CharacterState
{
  Stand,
  Walk,
  Jump,
  GrabLedge,
};

namespace Constants
{
  const float cWalkSpeed = 4.0f;
  const float cJumpSpeed = 6.0f;
  const float cMinJumpSpeed = 4.0f;
  const float cHalfSizeY = 0.5f;
  const float cHalfSizeX = 0.5f;
  const float cMaxFallingSpeed = 5.0f;
  const float cGravity = -9.81f;
};

namespace smlt
{

class Window;

namespace behaviours
{

class QuickCollisionPlayer : public QuickCollisionObject , public Managed<QuickCollisionPlayer>
{

public:
  QuickCollisionPlayer(Window *window);

  const std::string name() const
  {
    return "QuickCollisionPlayer";
  }

  void update(float dt);
  void fixed_update(float step);

  //Player control values
  CharacterState currentState;
  float jumpSpeed;
  float walkSpeed;

private:
  void on_behaviour_first_update(Organism *owner);
  void on_behaviour_added(Organism *controllable) override;
  void on_behaviour_removed(Organism *controllable) override;

  ActorPtr stage_node_ = nullptr;

  bool attached() const { return stage_node_ != nullptr; }

protected:
  //basic Wrapped Input Handling
  bool *inputs;
  bool *prevInputs;

  void UpdatePrevInputs();
  bool released(KeyInput key);
  bool key_state(KeyInput key);
  bool pressed(KeyInput key);
};
}
}
