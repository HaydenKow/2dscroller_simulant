/*
 * Filename: /mnt/hgfs/share/testproj2/sources/behaviors/QuickCollisionAABB.cpp
 * Path: /mnt/hgfs/share/testproj2/sources/behaviors
 * Created Date: Tuesday, January 8th 2019, 6:57:01 pm
 * Author: dev
 * 
 * Copyright (c) 2019 Your Company
 */

#pragma once

#include <simulant/simulant.h>
#include <simulant/window.h>
#include <simulant/behaviours/behaviour.h>
#include <simulant/interfaces/transformable.h>

struct QuickAABB
{
  smlt::Vec2 center;
  smlt::Vec2 halfSize;

  QuickAABB() : center(smlt::Vec2()), halfSize(smlt::Vec2()) {};
  QuickAABB(smlt::Vec2 _center, smlt::Vec2 _halfSize) : center(_center), halfSize(_halfSize) {};
  bool overlaps(QuickAABB other)
  {
      if ( abs(center.x - other.center.x) > halfSize.x + other.halfSize.x ) return false;
      if ( abs(center.y - other.center.y) > halfSize.y + other.halfSize.y ) return false;
      return true;
  }

};

namespace smlt
{

class Window;

namespace behaviours
{


class QuickCollisionManager : public Behaviour, public Managed<QuickCollisionManager>
{

  public:
    QuickCollisionManager();

    const std::string name() const
    {
        return "QuickCollisionManager";
    }

    void update(float dt);
    void fixed_update(float step);

  private:
    void on_behaviour_first_update(Organism *owner);
    void on_behaviour_added(Organism *controllable) override;
    void on_behaviour_removed(Organism *controllable) override;

    StageNode *stage_node_ = nullptr;

    bool attached() const { return stage_node_ != nullptr; }

  protected:
};
}
}
